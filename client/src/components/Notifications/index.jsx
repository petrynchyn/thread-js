import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { userType } from 'src/common/propTypes';
import 'react-notifications/lib/notifications.css';

const Notifications = ({ user, applyPost, reloadPost, deletePost, reloadExpandedPost }) => {
  const { REACT_APP_SOCKET_SERVER: address } = process.env;
  const [socket] = useState(io(address));

  useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socket.emit('createRoom', id);

    socket.on('like', (isLike = true) => {
      NotificationManager.info(`Your post was ${isLike ? '' : 'dis'}liked!`);
    });

    socket.on('like_comment', (isLike = true) => {
      NotificationManager.info(`Your comment was ${isLike ? '' : 'dis'}liked!`);
    });

    socket.on('reload_post', (userId, postId) => {
      if (userId !== id) {
        reloadPost(postId);
      }
    });

    socket.on('new_post', post => {
      if (post.userId !== id) {
        applyPost(post.id);
      }
    });

    socket.on('delete_post', post => {
      if (post.userId !== id) {
        deletePost(post.id, true); // local delete
      }
    });

    socket.on('expend_post', (uId, postId) => {
      if (uId !== user.id) { //
        reloadExpandedPost(postId);
      }
    });

    return () => {
      socket.close();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: userType,
  reloadExpandedPost: PropTypes.func.isRequired,
  applyPost: PropTypes.func.isRequired,
  reloadPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default Notifications;
