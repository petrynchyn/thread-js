const randomIndex = length => Math.floor(Math.random() * length);

export default {
  up: async (queryInterface, Sequelize) => {
    try {
      const options = {
        type: Sequelize.QueryTypes.SELECT
      };

      const users = await queryInterface.sequelize.query('SELECT id FROM "users";', options);
      const comments = await queryInterface.sequelize.query('SELECT id FROM "comments";', options);

      // Add comment reactions.
      const commentReactionsMappedSeed = users.map(user => ({
        isLike: true,
        createdAt: new Date(),
        updatedAt: new Date(),
        userId: user.id,
        commentId: comments[randomIndex(comments.length)].id
      }));
      await queryInterface.bulkInsert('commentReactions', commentReactionsMappedSeed, {});
    } catch (err) {
      console.info(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('commentReactions', null, {});
    } catch (err) {
      console.info(`Seeding error: ${err}`);
    }
  }
};
