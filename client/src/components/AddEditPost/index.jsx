import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const AddEditPost = ({
  savePost,
  uploadImage,
  editPost,
  post
}) => {
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState(post.image);
  const [isUploading, setIsUploading] = useState(false);

  const handleAddEditPost = async () => {
    if (!body) {
      return;
    }
    await savePost({ imageId: image?.id, body, id: post.id });
    if (editPost) {
      editPost();
    } else {
      setBody('');
      setImage(undefined);
    }
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id, link } = await uploadImage(target.files[0]);
      setImage({ id, link });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={handleAddEditPost} onReset={editPost}>
        {image?.link && (
          <div className={styles.imageWrapper}>
            <Image src={image?.link} alt="post" rounded fluid />
          </div>
        )}
        <Form.TextArea
          autoFocus
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        {(post.id)
          ? (
            <Button.Group floated="right">
              <Button type="reset">Cancel</Button>
              <Button.Or />
              <Button color="blue" type="submit">Save</Button>
            </Button.Group>
          ) : <Button floated="right" color="blue" type="submit">Post</Button>}
      </Form>
    </Segment>
  );
};

AddEditPost.propTypes = {
  savePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  editPost: PropTypes.func,
  post: PropTypes.objectOf(PropTypes.any)
};

AddEditPost.defaultProps = {
  post: { body: '', image: undefined, id: undefined },
  editPost: undefined
};

export default AddEditPost;
