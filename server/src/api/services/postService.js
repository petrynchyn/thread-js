import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const deletePost = async (userId, postId) => {
  const post = await postRepository.getPostById(postId);

  if (post.userId !== userId) throw Error('Forbidden delete someone else\'s post.');

  const deletedPost = await postRepository.deleteById(postId);

  return Number.isInteger(deletedPost) ? { id: postId, userId } : {};
};

export const update = async (userId, postId, body) => {
  let post = await postRepository.getPostById(postId);

  if (post.userId !== userId) throw Error('Forbidden edit someone else\'s post.');

  post = body;
  delete post.userId;

  const updatedPost = await postRepository.updateById(postId, post);

  return updatedPost;
};

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  const post = await postRepository.getPostById(postId);

  return Number.isInteger(result) ? { post } : { isLike, post };
};
