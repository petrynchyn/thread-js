import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  DEL_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const delPostAction = id => ({
  type: DEL_POST,
  id
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const reloadPost = postId => async (dispatch, getRootState) => {
  const { posts: { posts, expandedPost } } = getRootState();

  let updatedPost;

  const getPost = async () => {
    updatedPost = await postService.getPost(postId);
    return updatedPost;
  };

  const updated = await Promise.all(posts.map(post => (post.id !== postId ? post : getPost())));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const updatePost = post => async dispatch => {
  const updatedPost = await postService.updatePost(post);
  dispatch(reloadPost(updatedPost.id));
};

export const deletePost = (id, isLocal = false) => async (dispatch, getRootState) => {
  const updatedPost = !isLocal ? await postService.deletePost(id) : { id };
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === updatedPost?.id) {
    dispatch(setExpandedPostAction());
  }
  dispatch(delPostAction(updatedPost?.id));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const reloadExpandedPost = postId => async (dispatch, getRootState) => {
  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(toggleExpandedPost(postId));
  }
};

export const likePost = (postId, isLike = true) => async (dispatch, getRootState) => {
  const { post: updatedPost } = await postService.likePost(postId, isLike);

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : updatedPost));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const likeComment = (commentId, isLike = true) => async (dispatch, getRootState) => {
  const { comment: updatedComment } = await commentService.likeComment(commentId, isLike);

  const { posts: { expandedPost } } = getRootState();
  const updatedComments = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : updatedComment));

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    dispatch(setExpandedPostAction({ ...expandedPost, comments: updatedComments }));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const editComment = (commentId, request) => async (dispatch, getRootState) => {
  const comment = await commentService.editComment(commentId, request);
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === comment.postId) {
    const updatedPost = {
      ...expandedPost,
      comments: expandedPost.comments.map(cmnt => (cmnt.id === comment.id ? { ...cmnt, ...comment } : cmnt))
    };

    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  const comment = await commentService.deleteComment(commentId);
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === comment?.postId) {
    const updatedPost = {
      ...expandedPost,
      commentCount: expandedPost.commentCount - 1,
      comments: expandedPost.comments.filter(cmnt => cmnt.id !== comment.id)
    };

    dispatch(setExpandedPostAction(updatedPost));
  }
};
