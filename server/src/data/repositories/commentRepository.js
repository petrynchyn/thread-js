import sequelize from '../db/connection';
import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
class CommentRepository extends BaseRepository {
  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(`
            (SELECT json_agg(json_build_object('createdAt', "commentReaction"."createdAt", 'userName', "username",
            'image', (SELECT link FROM "images" as "image" WHERE "image"."id" = "user"."imageId")))
            FROM "commentReactions" as "commentReaction", "users" as "user"
            WHERE "commentReaction"."commentId" = "comment"."id" AND "commentReaction"."isLike" = true
            AND  "commentReaction"."userId" = "user"."id")`),
          'likedList']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: CommentReactionModel,
        attributes: []
      }]
    });
  }
}

export default new CommentRepository(CommentModel);
