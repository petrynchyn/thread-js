import PropTypes from 'prop-types';
import { imageType } from 'src/common/propTypes/image';

const whoLikedType = PropTypes.exact({
  likedList: PropTypes.exact({
    userName: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    image: imageType
  }).isRequired
});

export { whoLikedType };
