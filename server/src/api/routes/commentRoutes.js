import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.isLike !== undefined && (reaction.comment.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        req.io.to(reaction.comment.userId).emit('like_comment', reaction.isLike);
      }
      req.io.emit('expend_post', req.user.id, reaction.comment.postId);
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => commentService.edit(req.user.id, req.params.id, req.body)
    .then(comment => {
      req.io.emit('expend_post', comment.userId, comment.postId);
      res.send(comment);
    })
    .catch(next))
  .delete('/:commentId', (req, res, next) => commentService.deleteComment(req.user.id, req.params.commentId)
    .then(({ userId, postId, commentId }) => {
      if (commentId) {
        req.io.emit('reload_post', userId, postId);
        req.io.emit('expend_post', userId, postId);
        return res.send({ id: commentId, postId });
      }
      return res.end();
    })
    .catch(next));

export default router;
