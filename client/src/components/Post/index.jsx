import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { postType } from 'src/common/propTypes';
import WhoLiked from '../WhoLiked';

import styles from './styles.module.scss';

const Post = ({ post, likePost, toggleExpandedPost, sharePost, editPost, userId, delPost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    likedList,
    commentCount,
    createdAt,
    updatedAt
  } = post;
  const date = moment(createdAt).fromNow();
  const updatedDate = updatedAt !== createdAt && moment(updatedAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          content={<WhoLiked likedList={likedList} />}
          on="hover"
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          )}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id, false)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {user.id === userId
          && <Label icon="edit" basic size="small" as="a" className={styles.toolbarBtn} onClick={() => editPost(id)} />}
        {user.id === userId
          && <Label icon="trash" basic size="small" as="a" className={styles.toolbarBtn} onClick={() => delPost(id)} />}
        {updatedDate
          && <Label content={updatedDate} icon="edit outline" tag size="small" />}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  likePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  delPost: PropTypes.func.isRequired
};

export default Post;
