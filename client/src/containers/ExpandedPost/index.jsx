import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header, Button } from 'semantic-ui-react';
import moment from 'moment';
import { postType } from 'src/common/propTypes';
import {
  toggleExpandedPost, updatePost, deletePost, likePost,
  addComment, editComment, deleteComment, likeComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import AddEditPost from 'src/components/AddEditPost';

const ExpandedPost = ({
  post,
  sharePost,
  likePost: likePst,
  likeComment: likeCmt,
  toggleExpandedPost: toggle,
  addComment: add,
  editComment: edtComment,
  userId,
  updatePost: updPost,
  uploadImage,
  editPostId,
  editPost,
  delPost,
  deleteComment: delComment
}) => {
  const [delCommentId, setDelCommentId] = useState(false);

  return (
    <Modal centered={false} open onClose={async () => { await editPost(); toggle(); }}>
      {post
        ? (
          <Modal.Content>
            {(editPostId === post.id) ? (
              <AddEditPost
                key={post.id}
                post={post}
                editPost={editPost}
                savePost={updPost}
                uploadImage={uploadImage}
              />
            ) : (
              <Post
                post={post}
                likePost={likePst}
                toggleExpandedPost={toggle}
                sharePost={sharePost}
                key={post.id}
                userId={userId}
                editPost={editPost}
                delPost={delPost}
              />
            )}
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>Comments</Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    userId={userId}
                    editComment={edtComment}
                    delComment={setDelCommentId}
                    likeComment={likeCmt}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} />
              <Modal size="mini" open={!!delCommentId} onClose={() => setDelCommentId(false)}>
                <Modal.Header>Delete Your COMMENT</Modal.Header>
                <Modal.Content>Are you sure you want to delete your comment?</Modal.Content>
                <Modal.Actions>
                  <Button content="No" onClick={() => setDelCommentId(false)} />
                  <Button
                    content="Yes"
                    color="blue"
                    onClick={() => { delComment(delCommentId); setDelCommentId(false); }}
                  />
                </Modal.Actions>
              </Modal>
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: postType.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  editPost: PropTypes.func.isRequired,
  delPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  editPostId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.any
  ]),
  deleteComment: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  editPostId: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  toggleExpandedPost,
  updatePost,
  deletePost,
  likePost,
  addComment,
  editComment,
  deleteComment,
  likeComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ExpandedPost);
