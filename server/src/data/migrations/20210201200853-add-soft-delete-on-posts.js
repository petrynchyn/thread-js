module.exports = {
  up: (queryInterface, Sequelize) => queryInterface
    .addColumn('posts', 'deletedAt', { allowNull: true, type: Sequelize.DATE }),

  down: queryInterface => queryInterface.removeColumn('posts', 'deletedAt')
};
