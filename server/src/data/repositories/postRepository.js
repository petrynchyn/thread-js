import { Op } from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel, CommentModel, UserModel, ImageModel,
  PostReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      hideUserId,
      likedUserId
    } = filter;

    const where = {};
    const whereLiked = {};

    if (userId) {
      Object.assign(where, { userId });
    } else if (hideUserId) {
      Object.assign(where, { userId: { [Op.not]: hideUserId } });
    }
    if (likedUserId) {
      Object.assign(whereLiked, {
        [Op.and]: [
          { isLike: true },
          { userId: likedUserId }
        ]
      });
    }

    return this.model.findAll({
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(`
            (SELECT json_agg(json_build_object('createdAt', "postReaction"."createdAt", 'userName', "username",
            'image', (SELECT link FROM "images" as "image" WHERE "image"."id" = "user"."imageId")))
            FROM "postReactions" as "postReaction", "users" as "user"
            WHERE "postReaction"."postId" = "post"."id" AND "postReaction"."isLike" = true
            AND  "postReaction"."userId" = "user"."id")`),
          'likedList']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        duplicating: false,
        where: whereLiked,
        attributes: []
      }
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(`
            (SELECT json_agg(json_build_object('createdAt', "postReaction"."createdAt", 'userName', "username",
            'image', (SELECT link FROM "images" as "image" WHERE "image"."id" = "user"."imageId")))
            FROM "postReactions" as "postReaction", "users" as "user"
            WHERE "postReaction"."postId" = "post"."id" AND "postReaction"."isLike" = true
            AND  "postReaction"."userId" = "user"."id")`),
          'likedList']
        ]
      },
      include: [{
        model: CommentModel,
        attributes: {
          include: [
            [sequelize.literal(`
              (SELECT COUNT(*)
              FROM "commentReactions" as "commReactions"
              WHERE "comments"."id" = "commReactions"."commentId" AND "commReactions"."isLike" = true)`),
            'likeCount'],

            [sequelize.literal(`
              (SELECT COUNT(*)
              FROM "commentReactions" as "commReactions"
              WHERE "comments"."id" = "commReactions"."commentId" AND "commReactions"."isLike" = false)`),
            'dislikeCount'],

            [sequelize.literal(`
              (SELECT json_agg(json_build_object('createdAt', "commentReaction"."createdAt", 'userName', "username",
              'image', (SELECT link FROM "images" as "image" WHERE "image"."id" = "user"."imageId")))
              FROM "commentReactions" as "commentReaction", "users" as "user"
              WHERE "commentReaction"."commentId" = "comments"."id" AND "commentReaction"."isLike" = true
              AND  "commentReaction"."userId" = "user"."id")`),
            'likedList']
          ]
        },
        include: [{
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }]
    });
  }
}

export default new PostRepository(PostModel);
