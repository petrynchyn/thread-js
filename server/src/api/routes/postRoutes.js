import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.isLike !== undefined && (reaction.post.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        req.io.to(reaction.post.userId).emit('like', reaction.isLike);
      }
      req.io.emit('reload_post', req.user.id, reaction.post.id);
      return res.send(reaction);
    })
    .catch(next))
  .put('/:postId', (req, res, next) => postService.update(req.user.id, req.params.postId, req.body)
    .then(post => {
      req.io.emit('reload_post', post.userId, post.id);
      return res.send(post);
    })
    .catch(next))
  .delete('/:postId', (req, res, next) => postService.deletePost(req.user.id, req.params.postId)
    .then(post => {
      req.io.emit('delete_post', post);
      return res.send(post);
    })
    .catch(next));

export default router;
