import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const edit = async (userId, commentId, body) => {
  let comment = await getCommentById(commentId);

  if (comment.userId !== userId) throw Error('Forbidden edit someone else\'s coment.');

  comment = { body: body.body };
  const updatedComment = await commentRepository.updateById(commentId, comment);

  return updatedComment;
};

export const deleteComment = async (userId, commentId) => {
  const comment = await getCommentById(commentId);

  if (comment.userId !== userId) throw Error('Forbidden delete someone else\'s coment.');

  const deleted = await commentRepository.deleteById(commentId);

  return Number.isInteger(deleted) ? { userId, postId: comment.postId, commentId } : {};
};

export const setReaction = async (userId, { commentId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? commentReactionRepository.deleteById(react.id)
    : commentReactionRepository.updateById(react.id, { isLike }));

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  let comment = await getCommentById(commentId);
  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  comment = await getCommentById(commentId);

  return Number.isInteger(result) ? { comment } : { isLike, comment };
};
