import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Form, Button, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers';
import { commentType } from 'src/common/propTypes';
import WhoLiked from '../WhoLiked';

import styles from './styles.module.scss';

const Comment = ({
  comment: { id, body: initBody, createdAt, updatedAt, user, likeCount, dislikeCount, likedList },
  userId, editComment, delComment, likeComment
}) => {
  const [isEdited, setIsEdited] = useState(false);
  const [body, setBody] = useState(initBody);

  const handlerSubmit = async () => {
    await editComment(id, { body });
    setIsEdited(false);
  };

  const updatedDate = updatedAt !== createdAt && moment(updatedAt).fromNow();

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        {updatedDate && (
          <CommentUI.Metadata>
            <Icon name="edit outline" size="small" />
            {updatedDate}
          </CommentUI.Metadata>
        )}
        {isEdited
          ? (
            <Form onSubmit={handlerSubmit} onReset={() => (setBody(initBody) || setIsEdited(false))}>
              <Form.TextArea
                autoFocus
                value={body}
                onChange={ev => setBody(ev.target.value)}
              />
              <Button.Group floated="right">
                <Button type="reset">Cancel</Button>
                <Button.Or />
                <Button color="blue" type="submit" disabled={body === initBody}>Save</Button>
              </Button.Group>
            </Form>
          ) : (
            <CommentUI.Text content={initBody} />
          )}
        <CommentUI.Actions>
          <Popup
            content={<WhoLiked likedList={likedList} />}
            on="hover"
            trigger={(
              <CommentUI.Action onClick={() => likeComment(id)}>
                <Icon name="thumbs up" />
                {likeCount}
              </CommentUI.Action>
            )}
          />
          <CommentUI.Action onClick={() => likeComment(id, false)}>
            <Icon name="thumbs down" />
            {dislikeCount}
          </CommentUI.Action>
          {user.id === userId && !isEdited && (
            <>
              <CommentUI.Action><Icon name="edit" onClick={() => setIsEdited(true)} link /></CommentUI.Action>
              <CommentUI.Action><Icon name="trash" onClick={() => delComment(id)} link /></CommentUI.Action>
            </>
          )}
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  userId: PropTypes.string.isRequired,
  editComment: PropTypes.func.isRequired,
  delComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired
};

export default Comment;
