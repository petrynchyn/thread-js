import React, { useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddEditPost from 'src/components/AddEditPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { postType } from 'src/common/propTypes';
import { Checkbox, Loader, Modal, Button } from 'semantic-ui-react';
import { loadPosts, loadMorePosts, likePost, toggleExpandedPost, addPost, updatePost, deletePost } from './actions';

import styles from './styles.module.scss';

let firstSetLocalStates = true;

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  updatePost: updPost,
  deletePost: delPost,
  likePost: like,
  toggleExpandedPost: toggle
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [ownPosts, setOwnPosts] = useState({ show: false, hide: false, liked: false });
  const [editPostId, setEditPostId] = useState(undefined);
  const [delPostId, setDelPostId] = useState(false);

  const toggleShowOwnPosts = () => {
    setOwnPosts({ ...ownPosts, show: !ownPosts.show, hide: false });
  };

  const toggleHideOwnPosts = () => {
    setOwnPosts({ ...ownPosts, hide: !ownPosts.hide, show: false });
  };

  const toggleShowPostsLikedByMe = () => {
    setOwnPosts({ ...ownPosts, liked: !ownPosts.liked });
  };

  useEffect(() => {
    if (firstSetLocalStates) {
      firstSetLocalStates = false;
      return;
    }
    postsFilter.userId = ownPosts.show ? userId : undefined;
    postsFilter.hideUserId = ownPosts.hide ? userId : undefined;
    postsFilter.likedUserId = ownPosts.liked ? userId : undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  }, [ownPosts, userId]);

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const editPost = id => {
    setEditPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddEditPost savePost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={ownPosts.show}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          label="Show posts liked by me"
          checked={ownPosts.liked}
          onChange={toggleShowPostsLikedByMe}
        />
        <Checkbox
          toggle
          label="Hide my posts"
          checked={ownPosts.hide}
          onChange={toggleHideOwnPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          (editPostId === post.id) ? (
            <AddEditPost
              key={post.id}
              post={post}
              editPost={editPost}
              savePost={updPost}
              uploadImage={uploadImage}
            />
          ) : (
            <Post
              post={post}
              likePost={like}
              toggleExpandedPost={toggle}
              sharePost={sharePost}
              key={post.id}
              userId={userId}
              editPost={editPost}
              delPost={setDelPostId}
            />
          )))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          editPostId={editPostId}
          editPost={editPost}
          delPost={setDelPostId}
          uploadImage={uploadImage}
          sharePost={sharePost}
        />
      )}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
      <Modal size="mini" open={!!delPostId} onClose={() => setDelPostId(false)}>
        <Modal.Header>Delete Your POST</Modal.Header>
        <Modal.Content>Are you sure you want to delete your post?</Modal.Content>
        <Modal.Actions>
          <Button content="No" onClick={() => setDelPostId(false)} />
          <Button content="Yes" color="blue" onClick={() => { delPost(delPostId); setDelPostId(false); }} />
        </Modal.Actions>
      </Modal>
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: postType,
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  toggleExpandedPost,
  addPost,
  updatePost,
  deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
