import React from 'react';
import { List, Image } from 'semantic-ui-react';
import moment from 'moment';
import { whoLikedType } from 'src/common/propTypes';

const WhoLiked = ({ likedList }) => (
  <List relaxed>
    {likedList?.map(like => (
      <List.Item>
        <Image avatar src={like.image} />
        <List.Content header={like.userName} description={moment(like.createdAt).fromNow()} />
      </List.Item>
    )) || <List.Item><List.Content description="yet nobody likes" /></List.Item>}
  </List>
);

WhoLiked.propTypes = whoLikedType;

export default WhoLiked;
